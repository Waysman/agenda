#ifndef PESSOAJURIDICA_H
#define PESSOAJURIDICA_H

#include <iostream>
#include <string>
#include "pessoa.hpp"
class PessoaJuridica:public Pessoa{

	private:
		string cnpj;
	public:
		PessoaJuridica();
		~PessoaJuridica();		
		string getCnpj();
		void setCnpj(string cnpj);
};





#endif
