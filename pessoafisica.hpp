#ifndef PESSOFISICA_H
#define PESSOAFISICA_H	

#include <iostream>
#include <string>
#include "pessoa.hpp"

class PessoaFisica : public Pessoa{

	private:
		string cpf;
		string rg;

	public:
		PessoaFisica();

};

#endif
