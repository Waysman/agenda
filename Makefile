all: pessoa.o pessoafisica.o pessoajuridica.o main.o
	g++ -o main pessoa.o pessoafisica.o pessoajuridica.o main.o

pessoa.o: pessoa.cpp pessoa.hpp 
	g++ -c pessoa.cpp

pessoafisica.o: pessoafisica.cpp pessoafisica.hpp
	g++ -c pessoafisica.cpp

pessoajuridica.o: pessoajuridica.cpp pessoajuridica.hpp
	g++ -c pessoajuridica.cpp

main.o: main.cpp pessoa.hpp 
	g++ -c main.cpp

clean:
	rm -rf *.o
run:
	 ./main
